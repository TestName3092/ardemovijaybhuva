﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonHander : MonoBehaviour {

	public UnityEvent OnClick = new UnityEvent();

	void OnMouseDown(){
		Debug.Log ("On Click");
		OnClick.Invoke();
	}
}